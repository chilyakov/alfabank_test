package ru.chilyakov;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public final class YandexTranslateTest {

    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String API_KEY_STRING = "AQVN2Mp2D9dQEREX1L-gdrfS2joinlYItdu1yWI6";
    private static RequestSpecification request = null;
    private static Response response = null;
    
    @BeforeAll
    public static void setUp(){
        request = RestAssured.given();
        request.header("Content-Type", "application/json; charset=UTF-8");
        request.header("Authorization", "Api-Key " + API_KEY_STRING);
    }
    
    public void translateRequest(String sourceLanguageCode, String targetLanguageCode, String sourceText){
        JSONObject requestBody = new JSONObject();
        requestBody.put("sourceLanguageCode", sourceLanguageCode);
        requestBody.put("targetLanguageCode", targetLanguageCode);
        requestBody.put("texts", sourceText);
        request.body(requestBody.toString());
        response = request.post(URL);
    }
    
    public String getTranslationTextFromJSON(){
        return response.jsonPath().get("translations[0].text");
    }
    
    @Test
    public void translateEnglishToRussian() {

        translateRequest("en", "ru", "Hello World!");

        Assertions.assertEquals(response.getStatusCode(), 200);        
        Assertions.assertEquals(getTranslationTextFromJSON(), "������, ���!");
    }

    @Test
    public void translateRussianToEnglish() {
        
        translateRequest("ru", "en", "������, ���!");

        Assertions.assertEquals(response.getStatusCode(), 200);        
        Assertions.assertEquals(getTranslationTextFromJSON(), "Hello, world!");
    }
    
}
